<?php

namespace App\Models\PandChat;

use Illuminate\Database\Eloquent\Model;

class ChatArchive extends Model
{
    protected $table = 'pandchat_chats_archives';
    protected $fillable = ['messages', 'ip'];
    protected $casts = [
        'messages' => 'json'
    ];
}
