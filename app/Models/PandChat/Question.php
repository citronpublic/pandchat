<?php

namespace App\Models\PandChat;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'pandchat_questions';
    protected $fillable = ['parent_answer_id', 'message'];
}
