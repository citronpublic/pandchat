<?php

namespace App\Models\PandChat;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'pandchat_answers';
    protected $fillable = ['parent_question_id', 'message'];

}
