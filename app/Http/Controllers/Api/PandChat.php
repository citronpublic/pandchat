<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PandChat\Answer;
use App\Models\PandChat\ChatArchive;
use App\Models\PandChat\Question;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class PandChat extends Controller
{
    public function answer(Request $request): JsonResponse
    {
        $nextQuestion = Question::select('id', 'message')
            ->where('parent_answer_id', $request->input('answer'))
            ->first();

        $possibleAnswers = Answer::select('id', 'message')
            ->where('parent_question_id', $nextQuestion->getKey())
            ->get();

        return response()->json([
            'question' => $nextQuestion->toArray(),
            'answers' => $possibleAnswers->toArray(),
        ]);
    }

    public function log(Request $request): void
    {
        ChatArchive::create([
            'messages' => $request->input('messages'),
            'ip' => $request->ip()
        ]);
    }

    public function listPreviousChats(): JsonResponse
    {
        $chats = ChatArchive::orderBy('created_at', 'desc')->get();
        return response()->json($chats->toArray());
    }
}
