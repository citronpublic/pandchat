<?php

use Illuminate\Database\Seeder;
use App\Models\PandChat\Answer;
use App\Models\PandChat\Question;

class PandChatTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $initialQuestion = Question::create([
            'message' => 'Bonjour! En quoi puis je vous aider?'
        ]);

        $modulePerso = Answer::create([
            'parent_question_id' => $initialQuestion->getKey(),
            'message' => 'J’aimerais en savoir plus sur votre module de perso'
        ]);

        Question::create([
            'parent_answer_id' => $modulePerso->getKey(),
            'message' => 'Notre module de perso est incroyable pour de nombreuses raisons... Je vous invite à télécharger votre logo <a href="#">​ici</a>​.'
        ]);

        $pricing = Answer::create([
            'parent_question_id' => $initialQuestion->getKey(),
            'message' => 'Si je souhaite faire une demande de devis, aurais je un interlocuteur pour m’assister?'
        ]);

        $assistance = Question::create([
            'parent_answer_id' => $pricing->getKey(),
            'message' => 'Bien entendu! Notre équipe sera ravie de vous assister par téléphone au numéro suivant : 03 90 22 42 22'
        ]);

        $localization = Answer::create([
            'parent_question_id' => $assistance->getKey(),
            'message' => 'Très bien, votre service d’assistance est il basé en France?'
        ]);

        Question::create([
            'parent_answer_id' => $localization->getKey(),
            'message' => 'Assurément, nous sommes tous ici, basés à Strasbourg!'
        ]);
    }
}
