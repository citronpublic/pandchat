<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePandchatChatsArchives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pandchat_chats_archives', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->ipAddress('ip');
            $table->json('messages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pandchat_chats_archives');
    }
}
