# PandChat
This is a Laravel project, see [laravel.com/docs/5.8#configuration](https://laravel.com/docs/5.8#configuration) for setup.

Use command `php artisan migrate` to generate the database and `php artisan db:seed` to seed your database.

Check out the demo [here](https://pandchat.de-carpentier.fr)

# Routing
`GET /` : PandChat application  
`GET /api/list-previous-chats` : Chats history
