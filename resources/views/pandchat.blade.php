<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex, nofollow">

    <title>PandChat</title>

    <link data-n-head="true" rel="icon" type="image/x-icon" href="https://www.pandacola.com/favicon.ico"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>
<div id="app">
    <app></app>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
