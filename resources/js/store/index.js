import Vue from 'vue';
import Vuex from 'vuex';
import fetchApi from './lib/fetchApi';

Vue.use(Vuex);

const state = {
    me: {},
    messages: [],
    answers: []
};

const mutations = {
    setMe(state, {me}) {
        state.me = me;
    },
    addMessage(state, messages) {
        state.messages.push(...messages);
    },
    setMessages(state, messages) {
        state.messages = messages;
    },
    setAnswers(state, answers) {
        state.answers = answers;
    },
};

const actions = {
    async initChat({commit}) {
        const response = await fetchApi.post('answer', {
            answer: null
        });

        const message = {
            id: response.data.question.id,
            text: response.data.question.message,
            type: 'question'
        };

        commit('setMessages', [message]);
        commit('setAnswers', response.data.answers);
    },

    async answer({commit}, answer) {
        commit('addMessage', [{
            id: answer.id,
            text: answer.message,
            type: 'answer'
        }]);

        const response = await fetchApi.post('answer', {
            answer: answer.id
        });

        const message = {
            id: response.data.question.id,
            text: response.data.question.message,
            user: 'question'
        };

        commit('addMessage', [message]);
        commit('setAnswers', response.data.answers);

        if (!response.data.answers.length) {
            fetchApi.post('log', {
                messages: state.messages
            });
        }

    }
};

const getters = {
    getMyUuid: (state) => state.me.uuid,
    getChatMessages: (state) => state.messages,
    getPossiblesAnswers: (state) => state.answers,
};

export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions,
});
