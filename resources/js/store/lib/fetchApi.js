import axios from 'axios';

const fetchApi = axios.create({
    baseURL: '/api',
    timeout: 1000,
    headers: {'X-Requested-With': 'XMLHttpRequest'}
});

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    fetchApi.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


export default fetchApi;
