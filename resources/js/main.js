import App from './components/App.vue';
import Vue from 'vue';
import store from './store';

new Vue({
    el: '#app',
    store,
    render: h => h(App)
});
