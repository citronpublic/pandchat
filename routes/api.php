<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('answer', ['uses' => 'PandChat@answer']);
Route::post('log', ['uses' => 'PandChat@log']);
Route::get('list-previous-chats', ['uses' => 'PandChat@listPreviousChats']);
